public class App {
    public static void main(String[] args) throws Exception {
        boolean v1 = true;
        char v2 = 'b';
        byte v3 = 1;
        short v4 = 2;
        int v5 = 123456789;
        long v6 = 123L;
        float v7 = 12.4f;
        double v8 = 12.45d;
        System.out.println(v1);
        System.out.println(v2);
        System.out.println(v3);
        System.out.println(v4);
        System.out.println(v5);
        System.out.println(v6);
        System.out.println(v7);
        System.out.println(v8);

    }
}
