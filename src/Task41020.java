public class Task41020 {
    public static void main(String[] args) throws Exception {
        int[] a1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        char[] a2 = { 'a', 'b', 'c', 'd', 'f' };
        System.out.println("Các phần tử của mảng a1 ");
        for (int i = 0; i < a1.length; i++) {
            System.out.println("Phần tử thứ " + i + " = " + a1[i]);
        }
        System.out.println("Các phần tử của mảng a2 ");
        for (int i = 0; i < a2.length; i++) {
            System.out.println("Phần tử thứ " + i + " = " + a2[i]);
        }
        System.out.println("Mảng a2");
        System.out.println(a2);

    }

}
